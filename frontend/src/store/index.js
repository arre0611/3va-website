import Vue from 'vue'
import Vuex from 'vuex'

import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const state = {
  searchData: [],
  searchParam: {},
  doctors: [],
  cityData: [],
  selectedCity: {},
  selectedArea: '',
  areaData: [],
  specialityData: [],
  specialityDoctorsData: [],
  specialitySearch: false
}

const store = new Vuex.Store({
  // namespaced: true,
  state,
  getters,
  actions,
  mutations
})

export default store
