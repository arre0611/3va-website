import WordService from '../services/services'

export const getSearchData = (context, searchKeys) => {
  return WordService.getDoctors({ ...searchKeys }).then(function (searchData) {
    let _searchData = JSON.parse(searchData.data)
    context.commit('setSearchData', _searchData)
  })
}

export const getCitiesData = (context) => {
  return WordService.getCities().then(function (citiesData) {
    context.commit('setCityData', citiesData.data)
  })
}

export const getAreaData = (context, citySlug) => {
  return WordService.getAreas({...citySlug}).then(function (areaData) {
    context.commit('setAreaData', JSON.parse(areaData.data))
  })
}

export const getSpecialityData = (context, params) => {
  return WordService.getSpecialities({...params}).then(function (specialityData) {
    context.commit('setSpecialityData', JSON.parse(specialityData.data))
  })
}

export const getSpecialityDoctorsData = (context) => {
  return WordService.getSpecialityDoctorsData().then(function (specialityData) {
    context.commit('setSpecialityDoctorsData', JSON.parse(specialityData.data))
  })
}

export const getSpecialSearchData = (context, searchKeys) => {
  return WordService.getSpecialDoctors({ ...searchKeys }).then(function (searchData) {
    let _searchData = JSON.parse(searchData.data)
    context.commit('setSearchData', _searchData)
  })
}

export const saveReview = (context, saveData) => {
  return WordService.saveReview({ ...saveData })
}
