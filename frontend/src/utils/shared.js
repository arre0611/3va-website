export default {
  scrolledInElement: function (element) {
    const docViewTop = document.documentElement.scrollTop
    const docViewBottom = docViewTop + document.documentElement.clientHeight

    const elemTop = element.offsetTop
    const elemBottom = elemTop + element.offsetHeight
    return ((elemTop >= docViewTop) && (elemBottom <= docViewBottom))
  },
  scrolledInElementWithParent: function (parent, element) {
    const docViewTop = document.documentElement.scrollTop
    const docViewBottom = docViewTop + document.documentElement.clientHeight

    const elemTop = parent.offsetTop + element.offsetTop
    const elemBottom = elemTop + element.offsetHeight
    return ((elemTop >= docViewTop) && (elemBottom <= docViewBottom))
  },
  isMobileVersion: function () {
    if (navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/BlackBerry/i) ||
      navigator.userAgent.match(/Windows Phone/i)) {
      return true
    } else {
      return false
    }
  }
}
