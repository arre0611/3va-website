import Api from './api'

export default {
  getDoctors (params) {
    return Api().post('/search_doctor', params)
  },
  getCities () {
    return Api().get('/get_city')
  },
  getAreas (params) {
    return Api().post('/get_region_by_city', params)
  },
  getSpecialities (params) {
    return Api().post('/get_speciality', params)
  },
  getSpecialityDoctorsData () {
    return Api().post('/get_speciality_doctors')
  },
  getSpecialDoctors (params) {
    return Api().post('/special_search_doctor', params)
  },
  saveReview (params) {
    return Api().post('/save_review', params)
  }
}
