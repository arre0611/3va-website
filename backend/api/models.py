from django.db import models


class Doctor(models.Model):
    doctor_id = models.CharField(max_length=20)
    clinics_id = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.TextField()
    specialities = models.TextField()
    experience_year = models.CharField(max_length=10)
    price = models.IntegerField(default=0)
    rate_value = models.FloatField(default=0)
    education = models.TextField()
    qualification = models.TextField()
    work_experience = models.TextField()
    specializes_in_diseases = models.TextField()

    def __str__(self):
        return self.name


class Clinics(models.Model):
    clinics_id = models.CharField(max_length=20)
    phone = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    logo = models.URLField()
    lat = models.CharField(max_length=60)
    lng = models.CharField(max_length=60)
    city_slug = models.CharField(max_length=60)
    address = models.CharField(max_length=200)
    region_name = models.CharField(max_length=60)
    description = models.TextField()
    worktime = models.TextField()
    specialities = models.TextField()

    def __str__(self):
        return self.name


class Review(models.Model):
    doctor_id = models.CharField(max_length=20)
    author = models.CharField(max_length=100)
    text = models.TextField(blank=True, null=True)
    rating = models.FloatField(default=0)
    date = models.DateField()
    
    # def __str__(self):
    #     return self.name


class City(models.Model):
    city_name = models.CharField(max_length=200)
    city_slug = models.CharField(max_length=200)
    city_lat = models.CharField(max_length=60)
    city_lng = models.CharField(max_length=60)

    def __str__(self):
        return self.name
